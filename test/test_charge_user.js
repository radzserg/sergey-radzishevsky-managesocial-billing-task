import assert from 'assert';
import request from 'supertest';
import {createServer, closeServer} from './test_helper';
import Promise from 'bluebird';

describe('loading express', () => {
  let server, creteUserPromise;

  before(() => {
    server = createServer()
  });

  after((done) => {
    closeServer(done)
  });


  it('created new stripe customer', (done) => {
    const d = new Date();
    const ts = (d).getTime();
    const email = `mail_${ts}@mail.com`;
    const description = `test_user_${ts}`;
    const source = {
      object: 'card',
      amount: 100,
      number: '4242424242424242', // stripe test card number
      exp_month: 12,
      exp_year: d.getYear() + 1901,
      cvc: '123'
    };

    creteUserPromise = new Promise((resolve) => {
      request(server)
        .post('/customers')
        .send({description, email, source})
        .expect(201, done)
        .expect('Content-Type', /json/)
        .expect(res => {
          assert.equal(res.body.email, email);
          assert.equal(res.body.description, description);
          assert(res.body.id);
          assert.equal(res.body.account_balance, 0);
          resolve(res.body);
        });
    });
  });

  let chargePromises = [];
  [10, 20, 20].forEach((amount) => {
    it('charges user', (done) => {
      chargePromises.push(new Promise((resolve) => {
        creteUserPromise.then((user) => {
          const chargeData = {
            amount: amount * 100,
            customer: user.id,
          };

          request(server)
            .post('/charges')
            .send(chargeData)
            .expect(201, done)
            .expect('Content-Type', /json/)
            .expect(res => {
              assert(res.body.id);
              assert.equal(res.body.amount, chargeData.amount);
              resolve(res.body);
            });
        });
      }));
    });
  });


  it('gets charges for user', (done) => {
    Promise.all(chargePromises).then((results) => {
      const charge = results[0];
      const customerId = charge.customer;

      request(server)
        .get(`/charges?customer_id=${customerId}`)
        .expect(200, done)
        .expect('Content-Type', /json/)
        .expect(res => {
          const charges = res.body;
          let total = 0;

          assert.equal(charges.length, 3);
          charges.forEach((charge) => {
            assert(charge.id);
            assert.equal(charge.object, 'charge');
            assert.equal(charge.customer, customerId);
            total += charge.amount / 100;
          });
          assert.equal(total, 50);
        });

    });
  });


});
