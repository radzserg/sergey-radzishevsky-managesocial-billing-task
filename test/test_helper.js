let server;

export function createServer() {
  process.env.PORT = 3001;
  server = require('../index');
  return server;
}

export function closeServer(done) {
  server.close(done);
}
