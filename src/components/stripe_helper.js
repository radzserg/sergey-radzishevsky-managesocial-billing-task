import Stripe from 'stripe';
import Promise from 'bluebird';

const stripe = Stripe(process.env.STRIPE_API_KEY);

export default {

  /**
   * Creates stripe user
   * @param userData
   */
  createUser(userData) {
    return new Promise((resolve, reject) => {
      stripe.customers.create(userData, (err, customer) => {
        if (err) return reject(err);
        resolve(customer);
      });
    });
  },

  /**
   * Charges user
   * @param chargeData
   */
  charge(chargeData) {
    return new Promise((resolve, reject) => {
      stripe.charges.create(chargeData, (err, user) => {
        if (err) return reject(err);
        resolve(user);
      });
    });
  },

  /**
   * Retrieves user charges
   * @param customerId
   */
  listCharges(customer, limit = 3) {
    return new Promise((resolve, reject) => {
      stripe.charges.list({ customer, limit }, (err, charges) => {
        if (err) return reject(err);
        resolve(charges);
      });
    });
  }

};
