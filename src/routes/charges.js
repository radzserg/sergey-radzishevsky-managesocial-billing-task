import { Router } from 'express';
import stripeHelper from '../components/stripe_helper';

const router = Router();

router.post('/', (req, res) => {
  const { customer, amount } = req.body;
  const chargeData = {
    amount,
    customer,
    currency: 'usd',
    description: `Charge for ${amount} USD`
  };
  // @todo data validation

  stripeHelper
    .charge(chargeData)
    .then((charge) => {
      res.status(201).json(charge);
    })
    .catch((err) => {
      console.error(err);
      return res.status(500).json({ message: 'Sorry we cannot charge your account at this moment. Please try again later.' });
    });
});


router.get('/', (req, res) => {
  const { customer_id }  = req.query;

  stripeHelper
    .listCharges(customer_id, 10)
    .then((charges) => {
      res.status(200).json(charges.data);
    })
    .catch((err) => {
      console.error(err);
      return res.status(500).json({ message: 'Sorry we cannot get charges at this moment. Please try again later.' });
    });
});

export default router;
