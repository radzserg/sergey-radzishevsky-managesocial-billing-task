import { Router } from 'express';
import stripeHelper from '../components/stripe_helper';

const router = Router();

/* GET home page. */
router.post('/', (req, res) => {
  const userData = req.body;

  // @todo data validation

  stripeHelper
    .createUser(userData)
    .then((customer) => {
      const data = { id: customer.id, email: customer.email, description: customer.description,
        account_balance: customer.account_balance };

      res.status(201).json(data);
    })
    .catch((err) => {
      console.error(err);
      return res.status(500).json({ message: 'Sorry we cannot create account at this moment. Please try again later.' });
    });
});

export default router;
