## Requirements

NodeJs, I tested on stable v7.7.1.

Add .env file to root directory 

STRIPE_API_KEY=sk_test_you_key


## Node commands

```

  npm start - runs server 
  
  npm run dev-start - runs dev server using nodemon with autoreload.
  
  npm lint - runs eslint task
  
  npm test - run eslint and tests 
  
``` 

## Endpoint data examples 


```
# request
curl -H "Content-Type: application/json" -X POST -d '{"email": "mymail.com", "description": "Tom Lee", "source": {"object":"card","amount":100,"number":"4242424242424242","exp_month":12,"exp_year":2019,"cvc":"123"}}' http://localhost:3000/customers
response
{"id":"cus_AFEJvup3AXOr3K","email":"mymail.com","description":"Tom Lee","account_balance":0}

# charge user
request
curl -H "Content-Type: application/json" -X POST -d '{"amount": 100, "customer": "cus_AFEJvup3AXOr3K"}' http://localhost:3000/charges
response
{stripe_charge_object}

# retrieve charges list
curl -H "Content-Type: application/json" -X GET  http://localhost:3000/charges?customer_id=cus_AFEJvup3AXOr3K
[{stripe_charge_object}, ...]

```
